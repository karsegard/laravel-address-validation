<?php

// config for KDA/Laravel\AddressValidation
return [
    'key' => env('GOOGLE_MAPS_ADDRESS_VALIDATION_API_KEY', ''),
];
