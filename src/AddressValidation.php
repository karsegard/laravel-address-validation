<?php

namespace KDA\Laravel\AddressValidation;

//use Illuminate\Support\Facades\Blade;
class AddressValidation
{
    protected $client;

    protected $endpoint = 'https://addressvalidation.googleapis.com/v1:validateAddress';

    protected string $api_key;

    protected string $country;

    protected ?string $language_code=null;

    protected ?string $locality = null;

    protected ?string $postal_code = null;

    protected ?string $response_id = null;


    public function previousResponseId($response_id):static
    {
        $this->response_id = $response_id;
        return $this;
    }

    public function __construct($client)
    {
        $this->client = $client;
    }

    public function apiKey(string $api_key): static
    {
        $this->api_key = $api_key;

        return $this;
    }

    public function language(string $language): static
    {
        $this->language_code = $language;

        return $this;
    }

    public function country(string $country): static
    {
        $this->country = $country;

        return $this;
    }

    public function postalCode(string $postal_code): static
    {
        $this->postal_code = $postal_code;

        return $this;
    }

    public function locality(string $locality): static
    {
        $this->locality = $locality;

        return $this;
    }

    public function validateAddress(string|array $address)
    {
        $payload = $this->buildPayload($address);

        $responsePayload  = [];
        if(!blank($this->response_id)){
           $responsePayload = ['previousResponseId'=>$this->response_id];
        }
        
        $payload = array_merge(['address' => $payload],$responsePayload);
        $response = $this->client->request('POST', $this->endpoint.'?key='.$this->api_key, ['json' => $payload]);
        $response = $response->getBody();

        return (string)$response;
    }

    public function buildPayload(string|array $address)
    {
        if (is_string($address)) {
            $address = explode("\n", $address);
        }

        $payload = [];
        $payload['regionCode'] = $this->country;
        if ($this->postal_code) {
            $payload['postalCode'] = $this->postal_code;
        }
        if ($this->locality) {
            $payload['locality'] = $this->locality;
        }
        if ($this->language_code) {
            $payload['languageCode'] = $this->language_code;
        }
        $payload['addressLines'] = $address;
        
        return $payload;
    }
}
