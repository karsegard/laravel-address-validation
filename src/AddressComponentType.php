<?php

namespace KDA\Laravel\AddressValidation;

enum AddressComponentType: string
{
    case UNKNOWN = 'unknown';
    case STREET_NUMBER = 'street_number';
    case ROUTE = 'route';
    case POSTAL_CODE = 'postal_code';
    case LOCALITY = 'locality';
    case COUNTRY = 'country';
    case PREMISE = 'premise';
    case ADMINISTRATIVE_AREA_LEVEL_1 = 'administrative_area_level_1';
    case POI = 'point_of_interest';
    
    public function label()
    {
        return __('laravel-address-validation::address_component_types.'.$this->name);
    }
}
