<?php

namespace KDA\Laravel\AddressValidation;

use Arr;

class AddressValidationResult
{
    protected $result;

    public function __construct(string $result)
    {
        $this->result = json_decode($result, true);
    }

    public function getFormattedAddress()
    {
        //return $this->result->address->formattedAddress;
        return Arr::get($this->result, 'result.address.formattedAddress', null);
    }

    public function isAddressComplete(): bool
    {
        return Arr::get($this->result, 'result.verdict.addressComplete', false);
    }

    public function isAddressInferred(): bool
    {
        return Arr::get($this->result, 'result.verdict.hasInferredComponents', false);

    }

    public function hasUnconfirmedComponents(): bool
    {
        return Arr::get($this->result, 'result.verdict.hasUnconfirmedComponents', false);

    }

    public function getComponents()
    {
        return collect(Arr::get($this->result, 'result.address.addressComponents', []))->map(function ($item) {
            return new AddressComponent($item);
        });
    }

    public function getComponentsByType(AddressComponentType $type)
    {
        return $this->getComponents()->filter(function ($item) use ($type) {
            return $item->getType() == $type;
        });
    }

    public function getInferredComponents()
    {
        return $this->getComponents()->filter(function ($item)  {
            return $item->isInferred() == true;
        });
    }

    public function getUnConfirmedComponents()
    {
        return $this->getComponents()->filter(function ($item)  {
            return $item->isConfirmed() != true;
        });
    }

    public function getComponentByType(AddressComponentType $type)
    {
        return $this->getComponentsByType($type)->first();
    }

    public function getCorrectedComponents()
    {
        return $this->getComponents()->filter(function ($item) {
            return $item->isCorrected();
        });
    }

    public function hasCorrectedComponents()
    {
        return $this->getCorrectedComponents()->count() > 0;
    }

    public function hasReplacedComponents()
    {
        return $this->getReplacedComponents()->count() > 0;
    }


    public function getReplacedComponents()
    {
        return $this->getComponents()->filter(function ($item) {
            return $item->isReplaced();
        });
    }

    public function getAlteredComponents(){
        $replaced = $this->getReplacedComponents();
        $corrected = $this->getCorrectedComponents();
        return $replaced->merge($corrected);
    }

    public function getGeoPrecision():float
    {
        return Arr::get($this->result, 'result.geocode.featureSizeMeters', 0);
    }

    public function getGeoLocation(): ?array
    {
        return Arr::get($this->result, 'result.geocode.location', null);
    }

    public function getResponseId(){
        return Arr::get($this->result, 'responseId', null);

    }
}
