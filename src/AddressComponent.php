<?php

namespace KDA\Laravel\AddressValidation;

use Arr;

class AddressComponent
{
    protected $attributes = [];

    public function __construct($attributes)
    {
        $this->attributes = $attributes;
    }

    public function getValue()
    {
        return Arr::get($this->attributes, 'componentName.text', '');
    }

    public function getType(): AddressComponentType
    {
        return AddressComponentType::from(Arr::get($this->attributes, 'componentType', 'unknown'));
    }

    public function isInferred(): bool
    {
        return Arr::get($this->attributes, 'inferred', false);
    }

    public function isCorrected(): bool
    {
        return Arr::get($this->attributes, 'spellCorrected', false);
    }

    public function isReplaced(): bool
    {
        return Arr::get($this->attributes, 'replaced', false);
    }

    public function isConfirmed(): bool
    {
        return Arr::get($this->attributes, 'confirmationLevel', '') == 'CONFIRMED';

    }

    public function __get($name)
    {
        return $this->attributes[$name] ?? null;
    }
}
