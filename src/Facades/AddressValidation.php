<?php

namespace KDA\Laravel\AddressValidation\Facades;

use Illuminate\Support\Facades\Facade;

class AddressValidation extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::class;
    }
}
