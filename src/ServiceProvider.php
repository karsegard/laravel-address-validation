<?php

namespace KDA\Laravel\AddressValidation;

use GuzzleHttp\Client;
use KDA\Laravel\AddressValidation\AddressValidation as Library;
use KDA\Laravel\AddressValidation\Facades\AddressValidation as Facade;
//use Illuminate\Support\Facades\Blade;
use KDA\Laravel\PackageServiceProvider;
use KDA\Laravel\Traits\HasCommands;
use KDA\Laravel\Traits\HasConfig;

class ServiceProvider extends PackageServiceProvider
{
    use HasCommands;
    use HasConfig;

    protected $packageName = 'laravel-address-validation';

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    // trait \KDA\Laravel\Traits\HasConfig;
    //    registers config file as
    //      [file_relative_to_config_dir => namespace]
    protected $configDir = 'config';

    protected $configs = [
        'kda/address-validation.php' => 'kda.laravel-address-validation',
    ];

    public function register()
    {
        parent::register();
        $this->app->singleton(Facade::class, function () {
            $client = app(Client::class);

            return (new Library($client))
                ->apiKey(config('kda.laravel-address-validation.key'));
        });
    }

    /**
     * called after the trait were registered
     */
    public function postRegister()
    {
    }

    //called after the trait were booted
    protected function bootSelf()
    {
    }
}
