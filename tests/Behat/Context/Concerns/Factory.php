<?php

namespace KDA\Tests\Behat\Context\Concerns;

use Behat\Gherkin\Node\PyStringNode;

/**
 * Defines application features from the specific context.
 */
trait Factory
{
    protected $factory_attributes = [];

    protected $factory_record = null;

    protected $factory_records = null;

    /**
     * @Given clearing factory attributes
     */
    public function clearingFactoryAttributes()
    {
        $this->factory_attributes = [];
    }

    /**
     * @Given the following factory attributes
     */
    public function theFollowingFactoryAttributes(PyStringNode $string)
    {

        $this->factory_attributes = json_decode($string->getRaw(), true);
        $this->assertNotNull($this->factory_attributes);
    }

    /**
     * @Given the factory attribute :name with value :value
     */
    public function theFactoryAttributeWithValue($name, $value)
    {
        $this->factory_attributes[$name] = $value;
    }

    /**
     * @Given crafting :count records :class
     */
    public function craftingRecords($count, $class)
    {
        $this->factory_records = $class::factory()->count($count)->create($this->factory_attributes);
        $this->clearingFactoryAttributes();
    }

    /**
     * @Given crafting a record :class
     * @Given crafting a record
     */
    public function craftingARecord($class = null)
    {
        $class = $class ?? $this->model_class;
        $this->factory_record = $class::factory()->create($this->factory_attributes);
        $this->clearingFactoryAttributes();

    }

    /**
     * @Then The record is not null
     */
    public function theRecordIsNotNull()
    {
        $this->assertNotNull($this->factory_record);
    }

    /**
     * @Then the record property :prop is not null
     */
    public function theRecordPropertyIsNotNull($prop)
    {
        $this->assertNotNull($this->factory_record->$prop);
    }

    /**
     * @Then the record path :path is :value
     */
    public function theRecordPathIs($path, $value)
    {
        return \Arr::get($path, $this->factory_record) == $value;
    }
}
