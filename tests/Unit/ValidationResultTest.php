<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Laravel\AddressValidation\AddressComponentType;
use KDA\Laravel\AddressValidation\AddressValidationResult;
use KDA\Tests\TestCase;

class ValidationResultTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function inferred_success()
    {
        /*
         "address": {
        "regionCode": "CH",
        "addressLines": ["25 rue des deux-ponts"]
  }
   */
        $response = new AddressValidationResult($this->loadTestResponse('success_inferred'));

        $this->assertTrue($response->isAddressComplete());
        $this->assertTrue($response->isAddressInferred());
        $this->assertFalse($response->hasUnconfirmedComponents());
        $this->assertFalse($response->hasCorrectedComponents());

        // dump($response->getComponents()->first()->getValue());
        // dump($response->getComponentsByType(AddressComponentType::STREET_NUMBER));
    }

    /** @test */
    public function spell_corrected()
    {
        /*
         "address": {
        "regionCode": "CH",
        "locality": "Genev",
        "addressLines": ["25 rue des deux-ponts"]
  }
   */
        $response = new AddressValidationResult($this->loadTestResponse('spell_corrected'));

        $this->assertEquals($response->getFormattedAddress(), 'Rue des Deux-Ponts 25, 1205 Genève, Suisse');
        $this->assertTrue($response->isAddressComplete());
        $this->assertFalse($response->hasUnconfirmedComponents());
        $this->assertTrue($response->isAddressInferred());
        $this->assertTrue($response->hasCorrectedComponents());
        $this->assertTrue($response->getComponentByType(AddressComponentType::LOCALITY)->isCorrected());
    }

    /** @test */
    public function bad_address()
    {
        /*
          "address": {
         "regionCode": "CH",
          "locality": "",
        "addressLines": ["23 chemin des deux-ponts"]
   }
        */
        $response = new AddressValidationResult($this->loadTestResponse('bad_address'));

        $this->assertFalse($response->isAddressComplete());
        $this->assertTrue($response->hasUnconfirmedComponents());
        $this->assertFalse($response->isAddressInferred());
        $this->assertFalse($response->hasCorrectedComponents());
    }

    /** @test */
    public function bad_address_inferred()
    {
        /*
           "address": {
          "regionCode": "CH",
        "locality": "Geneve",
        "addressLines": ["23 chemin des deux-ponts"]
        }
         */
        $response = new AddressValidationResult($this->loadTestResponse('bad_address_inferred'));

        $this->assertTrue($response->isAddressComplete());
        $this->assertTrue($response->hasUnconfirmedComponents());
        $this->assertTrue($response->isAddressInferred());
        $this->assertFalse($response->hasCorrectedComponents());
        $this->assertFalse($response->getComponentByType(AddressComponentType::ROUTE)->isConfirmed());
        dump($response->getGeoPrecision());
        dump($response->getGeoLocation());
    }

    public function loadTestResponse($response)
    {
        return file_get_contents(__DIR__.'/../responses/'.$response.'.json');
    }
}
